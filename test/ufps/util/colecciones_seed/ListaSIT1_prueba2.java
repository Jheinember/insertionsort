/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MADARME
 */
public class ListaSIT1_prueba2 {
    
    public ListaSIT1_prueba2() {
    }

    @Test
    public void testInsertarAlInicio() {
        System.out.println("Probando InsergarAlInicio");
        //Escenario prueba:
        ListaS<Integer> l=new ListaS();
        l.insertarAlInicio(1);
        l.insertarAlInicio(2);
        
        //Lo esperado:
        //la cabeza de la lista contiene a quien: 2, después 1
        //Tamaño es cuanto 2
        
        boolean pasoPrueba=false;
        if(l.getTamanio()==2 && l.get(0)==2 && l.get(1)==1)
            pasoPrueba=true;
        
        assertTrue(pasoPrueba);
        
        
        
    }

    
    
    @Test
    public void testsortSelection() {
        System.out.println("Probando sortSelection");
        //Escenario prueba:
        ListaS<Integer> l=new ListaS();
        l.insertarAlInicio(10000);
        l.insertarAlInicio(100);
        l.insertarAlInicio(20000);
        
        //Lo esperado:
        //es una lista ordenada: 100->10000->20000->null
        //Tamaño es cuanto 3
        ListaS<Integer> l_esperado=new ListaS();
        l_esperado.insertarAlFinal(100);
        l_esperado.insertarAlFinal(10000);
        l_esperado.insertarAlFinal(20000);
        
        //Ejecutó la prueba:
        
        l.ordenarInsercion_Infos();
        
        boolean pasoPrueba=false;
        if(l_esperado.getTamanio()==3 && l_esperado.equals(l))
            pasoPrueba=true;
        
        assertTrue(pasoPrueba);
        
        
        
    }
   
    
}
