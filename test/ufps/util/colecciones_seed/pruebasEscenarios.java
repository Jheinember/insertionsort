/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

import java.util.Iterator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jheinember
 */
public class pruebasEscenarios {
    
    public pruebasEscenarios() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of insertarAlInicio method, of class ListaS.
     */
    @Test
    
    public void testInsertarAlFinal() {
        System.out.println("InsertarAlFinal");
     
        ListaS<Integer> instance = new ListaS<>();
        instance.insertarAlFinal(3);
        instance.insertarAlFinal(4);
        instance.insertarAlFinal(7);
        instance.insertarAlFinal(9);
        
        Object arr[]={3,4,7,9};
        assertEquals(arr, instance.aVector());        
    }    
    /**
     * Test of insertarAlInicio method, of class ListaS.
     */
    @Test
       public void testInsertarAlFinal2() {
        System.out.println("insertarAlFinal");
     
        ListaS<String> instance = new ListaS<>();
        instance.insertarAlFinal("casa");
        instance.insertarAlFinal("auto");
        instance.insertarAlFinal("jardin");
        instance.insertarAlFinal("patio");
        
        Object arr[]={"casa","auto","jardin","patio"};
        assertEquals(arr, instance.aVector());
        // TODO review the generated test code and remove the default call to fail.
        
    }
    /**
     * Test of insertarOrdenado method, of class ListaS.
     */
    @Test
    public void testInsertarOrdenado() {
       System.out.println("insertarAlFinal");
     
        ListaS<String> instance = new ListaS<>();
        instance.insertarOrdenado("casa");
        instance.insertarOrdenado("auto");
        instance.insertarOrdenado("jardin");
        instance.insertarOrdenado("vos");
        
        Object arr[]={"casa","auto","jardin","patio"};
        assertEquals(arr, instance.aVector());
    }

   /**
     * Test of insertarOrdenado method, of class ListaS.
     */
    @Test
    public void testInsertarOrdenado2() {
       System.out.println("insertarAlFinal");
     
        ListaS<Integer> instance = new ListaS<>();
        instance.insertarOrdenado(6);
        instance.insertarOrdenado(8);
        instance.insertarOrdenado(13);
        instance.insertarOrdenado(52);
        
        Object arr[]={6,8,13,45};
        assertEquals(arr, instance.aVector());
    }
    @Test
    public void testSet() {
      ListaS<Integer> instance = new ListaS<>();
        instance.insertarAlFinal(1);
        instance.insertarAlFinal(2);
        instance.insertarAlFinal(5);
        instance.insertarAlFinal(4);
        
        ListaS<Integer> i2 = new ListaS<>();
        i2.insertarAlFinal(1);
        i2.insertarAlFinal(2);
        i2.insertarAlFinal(3);
        i2.insertarAlFinal(4);
        
        instance.set(2, 3);
        
        assertEquals(i2, instance);
    }

    
    public void testSet2() {
      ListaS<String> instance = new ListaS<>();
        instance.insertarAlFinal("simple");
        instance.insertarAlFinal("estructura");
        instance.insertarAlFinal("enlazada");
        instance.insertarAlFinal("dobles");
        
        ListaS<String> i2 = new ListaS<>();
       
        i2.insertarAlFinal("simple");
        i2.insertarAlFinal("estructura");
        i2.insertarAlFinal("enlazada");
        i2.insertarAlFinal("dobles");
        i2.set(2, "enlazada");
        
        assertEquals(i2, instance);
    }


  

    /**
     * Test of ordenarInsercion_Infos method, of class ListaS.
     */
    @Test
    public void testSortSelection_Infos() {
            ListaS<String> i = new ListaS<>();
        i.insertarAlFinal("simple");
        i.insertarAlFinal("estructura");
        i.insertarAlFinal("enlazada");
        i.insertarAlFinal("dobles");
        
        
        
         ListaS<String> instance = new ListaS<>();
        instance.insertarAlFinal("estructura");
        instance.insertarAlFinal("enlazada");
        instance.insertarAlFinal("dobles");
        instance.insertarAlFinal("simple");
        instance.ordenarInsercion_Infos();
        assertEquals(instance, i);
    }

    /**
     * Test of ordenarInsercion_Nodos method, of class ListaS.
     */
    @Test
    public void testSortSelection_Nodos() throws Exception {
                   ListaS<String> i = new ListaS<>();
        i.insertarAlFinal("otros");
        i.insertarAlFinal("quizas");
        i.insertarAlFinal("talvez");
        i.insertarAlFinal("depronto");
        
        
        
         ListaS<String> instance = new ListaS<>();
        instance.insertarAlFinal("quizas");
        instance.insertarAlFinal("talvez");
        instance.insertarAlFinal("depronto");
        instance.insertarAlFinal("otros");
        instance.ordenarInsercion_Nodos();
        assertEquals(instance, i);
    }
    
       /**
     * Test of ordenarInsercion_Nodos method, of class ListaS.
     */
    @Test
    public void testSortSelection_Nodos2() throws Exception {
                   ListaS<Integer> i = new ListaS<>();
        i.insertarAlFinal(9);
        i.insertarAlFinal(5);
        i.insertarAlFinal(22);
        i.insertarAlFinal(100);
        
        
        
         ListaS<Integer> instance = new ListaS<>();
        instance.insertarAlFinal(100);
        instance.insertarAlFinal(22);
        instance.insertarAlFinal(9);
        instance.insertarAlFinal(5);
        instance.ordenarInsercion_Nodos();
        assertEquals(instance, i);
    }
}
  
