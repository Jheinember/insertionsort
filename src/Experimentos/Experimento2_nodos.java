/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Experimentos;

import ufps.util.colecciones_seed.ExceptionUFPS;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author Jheinember
 */
public class Experimento2_nodos {

    public static void main(String[] args) throws ExceptionUFPS {
        ListaS<Integer> l = new ListaS<>();
        for (int i = 1; i <= 200000; i++) {
            l.insertarAlInicio(i);
        }
        l.ordenarInsercion_Nodos();
    }
}
